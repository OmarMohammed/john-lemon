﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Explosion : MonoBehaviour
{
    //variable for how long explosion will be around
    float timearound;

    // Start is called before the first frame update
    void Start()
    {
        //Set the time
        timearound = 2f;
    }

    private void FixedUpdate()
    {
        //make it go down
        timearound -= Time.deltaTime;

        //if there is no time left
        if (timearound <= 0)
        {
            //remove the explosion
            Destroy(gameObject);
        }
    }


    //if anything is inside
    private void OnTriggerStay(Collider other)
    {
        //if it is an enemy or cracked wall
        if (other.gameObject.CompareTag("Enemy"))
        {
            other.gameObject.SetActive(false);
        }
        if (other.gameObject.CompareTag("CrackedWall"))
        {
            other.gameObject.SetActive(false);
        }
    }
}