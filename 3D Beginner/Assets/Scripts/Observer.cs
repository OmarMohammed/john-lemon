﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Observer : MonoBehaviour
{

    //Variable for player
    public Transform player;

    //Variable for ending
    public GameEnding gameEnding;

    //Variable for if the player is in ranfe
    bool m_IsPlayerInRange;

    //Check if the player is in range
    void OnTriggerEnter(Collider other)
    {
        if(other.transform == player)
        {
            //Set the variable on
            m_IsPlayerInRange = true;
        }
    }

    //If the player leaves the range
    private void OnTriggerExit(Collider other)
    {
        if (other.transform == player)
        {
            //Set the variable off
            m_IsPlayerInRange = false;
        }
    }
    private void Update()
    {
        //Check if the player is in range
        if (m_IsPlayerInRange)
        {
            //Rotate the Gargoyle 
            Vector3 direction = player.position - transform.position + Vector3.up;

            //Make sure the Ray does not go through walls
            Ray ray = new Ray(transform.position, direction);
            RaycastHit raycastHit;

            if(Physics.Raycast(ray, out raycastHit))
            {
                //if the raycast hits the player
                if(raycastHit.collider.transform == player)
                {
                    //Check if the Player is not invincible
                    PlayerMovement pMovement = player.GetComponent<PlayerMovement>();
                    if(!pMovement.invincibility)
                    {
                        //End game
                        gameEnding.CaughtPlayer();
                    }
                }
            }
        }
    }
}
