﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI; 

public class PlayerMovement : MonoBehaviour
{
    //setting the characters turnspeed
    public float turnSpeed = 20f;

    //Number of bombs player has
    public int Bombs;

    //Small delay between bomb placements to stop unwanted attacks
    float Bomb_Delay;

    //Object for the Explosion
    public GameObject m_Explosion;

    public Text bomb_UI;

    public Text speedLevel_UI;

    public Text speedUpgrade_UI;

    public Text frozenMessage;

    public Text invincibleMessage;

    //Variable for animator
    Animator m_Animator;

    //Variable for rigidbody
    Rigidbody m_Rigidbody;

    //Variable for audio
    AudioSource m_AudioSource;

    // Variable for player movement
    Vector3 m_Movement;

    //Variable for player rotation
    Quaternion m_Rotation = Quaternion.identity;

    // How many  speed power-ups the player must pickup to get the speed boost
    public int speedUpgrade;

    //allPatrols variable
    WaypointPatrol[] allPatrols;

    //Variable for wether invincibility is on or not
    public bool invincibility;

    //cooldown for invincibility
    public float invincibility_cooldown;

    public float freeze_Time;

    public bool frozen;

    // Start is called before the first frame update
    void Start()
    {
        //set values
        speedUpgrade = 5;

        Bombs = 0;

        Bomb_Delay = 0f;

        //Set the animator variable to the Animator instead of leaving it empty
        m_Animator = GetComponent<Animator>();

        //Set the rigidbody variable to the rigidbody instead of nothing
        m_Rigidbody = GetComponent<Rigidbody>();

        //Set audio variable
        m_AudioSource = GetComponent<AudioSource>();

        allPatrols = FindObjectsOfType<WaypointPatrol>();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        bomb_UI.text = "Bombs : " + Bombs;

        speedLevel_UI.text = "Speed Level : " + m_Animator.speed;

        speedUpgrade_UI.text = "Speed power-ups until upgrade : " + speedUpgrade;

        frozenMessage.text = "Enemies are frozen for : " + freeze_Time;

        invincibleMessage.text = "You are invincible for : " + invincibility_cooldown;

        if (frozen)
        {
            freeze_Time -= Time.deltaTime;
            frozenMessage.gameObject.SetActive(true);
        }

        //Do extra if the play is Invincible
        if (invincibility)
        {
            //Lower the cooldown by Time.deltaTime
            invincibility_cooldown -= Time.deltaTime;
            invincibleMessage.gameObject.SetActive(true);
            if (invincibility_cooldown <= 0)
            {
                invincibleMessage.gameObject.SetActive(false);
                invincibility = false;
            }
        }
        //Set the values for player movement
        float horizontal = Input.GetAxis("Horizontal");
        float vertical = Input.GetAxis("Vertical");

        //Check if the bomb attack is happening, if the delay is above zero, and if the player has any bombs
        if (Input.GetKey(KeyCode.Space) && Bomb_Delay <= 0 && Bombs > 0)
        {
            BombAttack();
        }
        // Count down bomb delay if there is any
        Bomb_Delay -= Time.deltaTime;

        //Put the values for player movement into a Vector3
        m_Movement.Set(horizontal, 0f, vertical);

        //Normalize it so the player does not move faster diagonally
        m_Movement.Normalize();

        //Check if the player is moving, for the animation
        bool hasHorizontalInput = !Mathf.Approximately(horizontal, 0f);
        bool hasVerticalInput = !Mathf.Approximately(vertical, 0f);
        bool isWalking = hasHorizontalInput || hasVerticalInput;

        //Sets the walking animation if the player is walking
        m_Animator.SetBool("IsWalking", isWalking);

        //Check if the player is walking
        if (isWalking)
        {
            //Check if the audio is alreay playing
            if (!m_AudioSource.isPlaying)
            {
                //Play the audio if the player is walking and it is not already playing
                m_AudioSource.Play();
            }
        }
        //If the player is not walking
        else
        {
            //Stop the audio
            m_AudioSource.Stop();
        }

        //Sets Rotation to a value
        Vector3 desiredForward = Vector3.RotateTowards(transform.forward, m_Movement, turnSpeed * Time.deltaTime, 0f);
        m_Rotation = Quaternion.LookRotation(desiredForward);
    }

    private void OnAnimatorMove()
    {
        //Moves player
        m_Rigidbody.MovePosition(m_Rigidbody.position + m_Movement * m_Animator.deltaPosition.magnitude);

        //Rotates player
        m_Rigidbody.MoveRotation(m_Rotation);
    }

    //Function to Freeze Enemies
    void FreezeEnemies()
    {
        foreach (WaypointPatrol patrol in allPatrols)
        {
            //Make sure it isn't null
            if (patrol.gameObject.activeInHierarchy)
            {
                //stop the movement
                patrol.navMeshAgent.isStopped = true;
            }
        }
        //Cancel Invoke to stop bugs from stacking them
        CancelInvoke("EndFreeze");

        freeze_Time = 7f;

        frozen = true;

        //Start time for how long until the freeze ends
        Invoke("EndFreeze", 7f);
    }

    //function to end freeze
    void EndFreeze()
    {
        foreach (WaypointPatrol patrol in allPatrols)
        {
            //check to make sure it isn't null
            if (patrol != null)
            {
                //start movement
                patrol.navMeshAgent.isStopped = false;
            }
        }
        frozen = false;
        frozenMessage.gameObject.SetActive(false);
    }
    //function for bomb attack
    void BombAttack()
    {
        // Start delay before being able to do it again
        Bomb_Delay = .25f;

        //Remove a bomb
        Bombs -= 1;

        //Create the explosion
        Instantiate(m_Explosion, transform.position, transform.rotation);
    }

    //Check for power ups
    private void OnTriggerEnter(Collider other)
    {
        //if speed
        if (other.gameObject.CompareTag("speedPowerUp"))
        {
            //destroy it
            other.gameObject.SetActive(false);

            //make speed upgrade go down one
            speedUpgrade -= 1;

            //if at zero
            if (speedUpgrade <= 0)
            {
                //reset upgrade requirement
                speedUpgrade = 5;

                //give the player a speed boost of one
                m_Animator.speed += 1;
            }
        }
        //if freeze
        if (other.gameObject.CompareTag("freezePowerUp"))
        {
            //destroy it
            other.gameObject.SetActive(false);

            //start freeze process
            FreezeEnemies();
        }
        //if a bomb
        if (other.gameObject.CompareTag("Bomb"))
        {
            //destory it
            other.gameObject.SetActive(false);

            //add it to the player
            Bombs += 1;

        }
        //if an invincibility powerup
        if (other.gameObject.CompareTag("invincibilityPowerUp"))
        {
            //destory it
            other.gameObject.SetActive(false);

            //make the player invincible
            invincibility = true;

            //Start the cooldown
            invincibility_cooldown = 5f;

        }
    }
}
