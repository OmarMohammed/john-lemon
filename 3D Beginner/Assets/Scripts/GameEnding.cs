﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameEnding : MonoBehaviour
{
    //Time it takes for it to fade away
    public float fadeDuration = 1f;

    //Amount of time the Image stays up
    public float displayImageDuration = 1f;

    //Variable for the player
    public GameObject Player;

    //Variable for exitBackgroundImage
    public CanvasGroup exitBackgroundImageCanvasGroup;

    //Variable for exit audio
    public AudioSource exitAudio;

    //Variable for caughtBackgroundImage
    public CanvasGroup caughtBackgroundImageCanvasGroup;

    //Variable for audio when caught
    public AudioSource caughtAudio;

    //Variable that is true if the player is in the area
    bool m_IsPlayerAtExit;

    //Variable for if the player is caught
    bool m_IsPlayerCaught;

    //Timer
    float m_Timer;

    //To check if the audio has been played
    bool m_HasAudioPlayed;

    //Check if anything enters the area
    private void OnTriggerEnter(Collider other)
    {
        //Check if that thing is the player
        if(other.gameObject == Player)
        {
            //If it is set the variable to true
            m_IsPlayerAtExit = true;
        }
    }

    //If the player is caught set the variable to true
    public void CaughtPlayer()
    {
        m_IsPlayerCaught = true;
    }

    private void Update()
    {
        //If the player is at the end, end the level and game
        if (m_IsPlayerAtExit)
        {
            EndLevel(exitBackgroundImageCanvasGroup, false, exitAudio);
        }
        //If the player us caught, respawn the player
        else if(m_IsPlayerCaught)
        {
            EndLevel(caughtBackgroundImageCanvasGroup, true, caughtAudio);
        }
    }

    void EndLevel(CanvasGroup imageCanvasGroup, bool doRestart, AudioSource audioSource)
    {
        //check if the audio has played before
        if (!m_HasAudioPlayed)
        {
            //if not play it and set has played audio to true
            audioSource.Play();
            m_HasAudioPlayed = true;
        }
        //Sets the timer to actual time
        m_Timer += Time.deltaTime;

        //Makes the image fade in
        imageCanvasGroup.alpha = m_Timer / fadeDuration;

        //Code will only happen after both the fade duration and image display duration
        if(m_Timer > fadeDuration + displayImageDuration)
        {
            //checks if the game should restart or end
            if(doRestart)
            {
                SceneManager.LoadScene(0);
            }
            else
            {
                Application.Quit();
            }
        }
    }
}
