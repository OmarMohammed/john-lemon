﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyWorld : MonoBehaviour
{
    public GameObject Player;

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject != Player && other.gameObject.activeInHierarchy)
        {
            other.gameObject.SetActive(false);
        }
    }
}
