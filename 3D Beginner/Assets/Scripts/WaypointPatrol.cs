﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class WaypointPatrol : MonoBehaviour
{
    //Variable for NavMesh Agent
    public NavMeshAgent navMeshAgent;

    //Variable for waypoints
    public Transform[] waypoints;

    //Current waypoint
    int m_CurrentWaypointIndex;

    void Start()
    {
        //Start going towards the first waypoint
        navMeshAgent.SetDestination(waypoints[0].position);
    }

    void Update()
    {
        //If in stopping distance
        if (navMeshAgent.remainingDistance < navMeshAgent.stoppingDistance)
        {
            //Go to the next waypoint
            m_CurrentWaypointIndex = (m_CurrentWaypointIndex + 1) % waypoints.Length;
            navMeshAgent.SetDestination(waypoints[m_CurrentWaypointIndex].position);
        }
    }
}
